package com.example.contact;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.contact.bookcontentprovider.BookProvider;

public class BookContentProviderActivity extends AppCompatActivity {
    private Button btnSaveBook;
    private Button btnRead;
    private EditText etTitle;
    private EditText etISBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_content_provider);

        initView();
        initEvent();
    }

    private void initEvent() {
        btnSaveBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = etTitle.getText().toString();
                String isbn = etISBN.getText().toString();

                SaveBook(title,isbn);
            }
        });
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReadBook();
            }
        });

    }

    private void ReadBook() {
        //Uri uri = Uri.parse(BookProvider.CONTENT_URI+"/2");
        Cursor cursor;
        CursorLoader loader = new CursorLoader(
                this,
                BookProvider.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        cursor = loader.loadInBackground();
        String str = "";

        while(cursor != null && cursor.moveToNext()){
            String id = cursor.getString(cursor.getColumnIndex(BookProvider._ID));
            String title = cursor.getString(cursor.getColumnIndex(BookProvider.TITTLE));
            String isbn = cursor.getString(cursor.getColumnIndex(BookProvider.ISBN));

            str = "{ "+id+ ","+title + "," + isbn+"}";
        }
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    private void SaveBook(String title, String isbn) {
        ContentValues values = new ContentValues();
        values.put(BookProvider.TITTLE,title);
        values.put(BookProvider.ISBN,isbn);

        Uri uri = getContentResolver().insert(
                BookProvider.CONTENT_URI,values
        );
        Toast.makeText(this, uri.toString(), Toast.LENGTH_SHORT).show();
    }

    private void initView() {
        btnSaveBook = findViewById(R.id.btnSaveBook);
        btnRead = findViewById(R.id.btnRead);
        etTitle = findViewById(R.id.etTitle);
        etISBN = findViewById(R.id.etISBN);
    }
}
