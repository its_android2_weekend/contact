package com.example.contact.bookcontentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.icu.text.CaseMap;
import android.icu.text.IDNA;
import android.net.Uri;
import android.os.Build;

import java.net.URI;

import static android.content.ContentResolver.CURSOR_DIR_BASE_TYPE;
import static android.content.ContentResolver.CURSOR_ITEM_BASE_TYPE;

public class BookProvider extends ContentProvider {
    private static final String PROVIDER_NAME = "com.example.provider.books";
    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME+"/books");

    public static final String _ID = "_id";
    public static final String TITTLE = "title";
    public static final String ISBN = "isbn";

    private static final UriMatcher uriMatcher;
    private static final int BOOKS = 1;
    private static final int BOOK_ID = 2;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME,"books",BOOKS);
        uriMatcher.addURI(PROVIDER_NAME,"books/#",BOOK_ID);
    }

    //Database Field
    private static final String DATABSE_NAME = "books";
    private static final String TABLE_NAME = "tblbook";
    private static int DATABASE_VERSION = 1;
    private static final String CREATE_TABLE_BOOK =
            "CREATE TABLE " + TABLE_NAME +
                    " ("+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TITTLE + " TEXT NOT NULL, "+
                    ISBN + " TEXT NOT NULL )";
    private SQLiteDatabase dbBooks;


    private static class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context) {
            super(context, DATABSE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_BOOK);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }

    @Override
    public boolean onCreate() {
        DatabaseHelper dbHelper = new DatabaseHelper(getContext());
        dbBooks = dbHelper.getWritableDatabase();
        return (dbBooks == null)?false:true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectArgs, String sort) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);
        Cursor c;
        if(uriMatcher.match(uri)== BOOK_ID)
            builder.appendWhere(_ID + " = " + uri.getPathSegments().get(1));

        c = builder.query(
                dbBooks,
                projection,
                selection,
                selectArgs,
                null,
                null,
                sort);

        c.setNotificationUri(getContext().getContentResolver(),uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case BOOKS:
                return CURSOR_DIR_BASE_TYPE + "/vnd.example.books";
            case BOOK_ID:
                return CURSOR_ITEM_BASE_TYPE + "/vnd.example.books";
            default:
                throw new IllegalArgumentException("Invalid URI"+ uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long rowId = dbBooks.insert(TABLE_NAME,null,contentValues);
        if(rowId>0){
            Uri contentUri = ContentUris.withAppendedId(CONTENT_URI,rowId);
            getContext().getContentResolver().notifyChange(contentUri,null);
            return contentUri;
        }throw new IllegalArgumentException("Invalid Uri = "+ uri);
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
