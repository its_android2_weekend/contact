package com.example.contact;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.contact.Adapter.ContactAdapter;
import com.example.contact.Adapter.ContactListener;
import com.example.contact.Entity.Contact;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_READ_CONTACT = 168;
    private List<Contact> contactList;
    Uri allContact = Uri.parse(ContactsContract.Contacts.CONTENT_URI.toString());

    //View;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        //ReadContact();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //Check Permission;
            int check = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
            if(check == PackageManager.PERMISSION_GRANTED){
                ReadContact();
            }else{
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},REQUEST_READ_CONTACT);
            }
        }else{
            ReadContact();
            Log.e("URI",allContact.toString());
        }
    }
    private void ReadContact(){
        contactList = new ArrayList<>();
        ArrayList<String> arrayList = new ArrayList<>();

        Cursor cursor;
        CursorLoader cursorLoader = new CursorLoader(
                this,
                allContact,
                null,
                null,
                null,
                null
        );
        cursor = cursorLoader.loadInBackground();
        //Get Data
        while (cursor !=null && cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            Contact contact = new Contact(id,name);
            contactList.add(contact);
        }
        //Call data in Recyler;
        ContactAdapter adapter = new ContactAdapter();
        adapter.setOnClickListener(this);
        adapter.addMoreItem(contactList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRecylerViewClick(int position) {

        Contact contact = contactList.get(position);
        //Getting Phone Number;
        Cursor phoneCursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{String.valueOf(contact.getId())},
                null
        );
        String strPhoneNumber=null;
        while (phoneCursor !=null && phoneCursor.moveToNext()){
            strPhoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        Toast.makeText(this, contact.getDisplay_name() +" = "+strPhoneNumber, Toast.LENGTH_SHORT).show();


    }
}
